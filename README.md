# S3 USER MANAGED BUCKET MODULE

Creates a S3 bucket and an IAM user/key with access to the bucket

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| bucket\_name | Name of s3 bucket. | `string` | n/a | yes |
| user\_name | Name of s3 bucket. | `string` | s3-user | yes |

## Outputs

| Name | Description |
|------|-------------|
| user\_arn | Arn of the user that was created. |
| user\_name | Name of the service account user that was created. |
| bucket\_arn | Arn of the bucket that was created. |
| bucket\_name | Name of the bucket. |
| iam\_access\_key\_id | Access key. |
| iam\_access\_key\_secret | Access key secret. |
