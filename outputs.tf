output "user_arn" {
  description   = "the arn of the user that was created"
  value         = aws_iam_user.user.arn
}

output "user_name" {
  description   = "the name of the service account user that was created"
  value         = aws_iam_user.user.name
}

output "bucket_arn" {
  description   = "the arn of the bucket that was created"
  value         = aws_s3_bucket.bucket.arn
}

output "bucket_name" {
  description   = "the name of the bucket"
  value         = aws_s3_bucket.bucket.id
}

output "iam_access_key_id" {
  description   = "the access key"
  value         = aws_iam_access_key.user_keys.id
}

output "iam_access_key_secret" {
  description   = "the access key secret"
  value         = aws_iam_access_key.user_keys.secret
}