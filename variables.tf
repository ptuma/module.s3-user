variable "bucket_name" {
  description = "Name of s3 bucket"
  default     = ""
  type        = string
}

variable "user_name" {
  description = "Name of s3 user"
  default     = "s3-user"
  type        = string
}
