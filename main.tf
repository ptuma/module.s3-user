resource "aws_iam_user" "user" {
  name = var.user_name
}

resource "aws_iam_access_key" "user_keys" {
  user = aws_iam_user.user.name
}

resource "aws_s3_bucket" "bucket" {
  bucket        = var.bucket_name
  force_destroy = "true"
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.bucket.id

  policy = <<EOF
{
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "${aws_iam_user.user.arn}"
      },
      "Action": [ "s3:*" ],
      "Resource": [
        "${aws_s3_bucket.bucket.arn}",
        "${aws_s3_bucket.bucket.arn}/*"
      ]
    }
  ]
}
EOF
}